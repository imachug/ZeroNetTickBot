from github import Github
import time
import os
import re


github = Github(os.environ["ACCESSKEY"])

start = time.time()

while time.time() < start + 60 * 40:  # 40 min
    issue = github.get_repo("HelloZeroNet/ZeroNet").get_issue(2273)

    body = issue.body.strip().replace("\r", "").split("\n")

    for comment in issue.get_comments():
        topic = re.sub(r"[\s.]", "", comment.body.replace("\r", "").split("\n")[0]).lower()

        marker = "[ ]" if topic == "none" else "[x]"
        topic = {
            "gplv3andlax": "GPLv3 and Lax",
            "gplv3+andlax": "GPLv3+ and Lax",
            "gplv3-onlyandlax": "GPLv3-only and Lax",
            "gplv3+": "GPLv3+",
            "gplv3-only": "GPLv3-only",
            "gplv3": "GPLv3",
            "agplv3": "AGPLv3",
            "mit/bsd2": "MIT/BSD2",
            "bsd3": "BSD3",
            "apache2": "Apache2",
            "lax": "Lax",
            "none": "**Blocking**",
            "idon'tcare": "Doesn't care"
        }.get(topic)

        if not topic:
            continue

        try:
            idx = [
                line.strip().replace("[x]", "[ ]").replace("~~", "").startswith("- [ ] @" + comment.user.login + ":") or
                line.strip().replace("[x]", "[ ]").replace("~~", "") == "- [ ] @" + comment.user.login
                for line in body
            ].index(True)
        except ValueError:
            body.append("- " + marker + " ~~@" + comment.user.login + "~~: " + topic)
        else:
            body[idx] = "- " + marker + " ~~@" + comment.user.login + "~~: " + topic

    # Parse statistics
    stats = {
        "GPLv3+": 0,
        "GPLv3-only": 0,
        "AGPLv3": 0,
        "MIT/BSD2": 0,
        "BSD3": 0,
        "Apache2": 0,
        "**Blocking**": 0,
        "No reply": 0
    }
    total = 0
    for line in body[body.index("## Contributor list") + 1:body.index("## Passing people")]:
        match = re.match(r"^- \[[ x]\] (?:~~)?@([\w-]+)(?:~~)?(?:: (.+))?", line)
        if match:
            response = match.group(2) or "No reply"
            if response.replace(" and Lax", "") in ("GPLv3", "GPLv3+", "MIT/BSD2", "BSD3", "Lax", "Doesn't care"):
                stats["GPLv3+"] += 1
            if response.replace(" and Lax", "") in ("GPLv3", "GPLv3+", "GPLv3-only", "MIT/BSD2", "BSD3", "Lax", "Doesn't care"):
                stats["GPLv3-only"] += 1
            if response.replace(" and Lax", "") in ("AGPLv3", "MIT/BSD2", "BSD3", "Lax"):
                stats["AGPLv3"] += 1
            if "Lax" in response or response in ("MIT/BSD2", "Doesn't care"):
                stats["MIT/BSD2"] += 1
            if "Lax" in response or response in ("BSD3", "Doesn't care"):
                stats["BSD3"] += 1
            if "Lax" in response or response in ("Apache2", "Doesn't care"):
                stats["Apache2"] += 1
            if response == "**Blocking**":
                stats["**Blocking**"] += 1
            if response == "No reply":
                stats["No reply"] += 1
            total += 1

    for key, value in stats.items():
        percentage = value / total * 100 if total > 0 else 0
        idx = [line.startswith("- " + key + ": ") for line in body].index(True)
        body[idx] = "- " + key + ": " + str(round(percentage, 1)) + "% (" + str(value) + ")"

    issue.edit(body="\r\n".join(body))

    time.sleep(10)
